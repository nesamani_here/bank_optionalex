package com.example.demo.entities;

import java.util.Date;

public class Payment {

	private int id;
	private Date paymentDate;
	private String paymentType;
	private double amount;
	private int customerID;	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public Payment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Payment(int id, Date paymentDate, String paymentType, double amount, int customerID) {
		super();
		this.id = id;
		this.paymentDate = paymentDate;
		this.paymentType = paymentType;
		this.amount = amount;
		this.customerID = customerID;
	}
	
	@Override
	public String toString() {
		return "Payment [paymentType=" + paymentType + "]";
	}
	
}
